// Functie pentru controlul unui motor
// Intrare: pinii m1 si m2, directia si viteza

void StartMotor (int m1, int m2, int forward, int speed)
{
  if (speed == 0) // oprire
  {
    digitalWrite(m1, 0);
    digitalWrite(m2, 0);
  }
  else
  {
    if (forward)
    {
      digitalWrite(m2, 0);
      analogWrite (m1, speed); // folosire PWM
    }
    else
    {
      digitalWrite(m1, 0);
      analogWrite(m2, speed);
    }
  }
}

void start_motors(int direction_var) {
  // Acum se pornesc motoarele DC
  StartMotor (mpin00, mpin01, direction_var, velocity + 5);
  StartMotor (mpin10, mpin11, direction_var, velocity);
}

// Functie de siguranta
// Executa oprire motoare, urmata de delay
void delayStopped(int ms)
{
  StartMotor (mpin00, mpin01, 0, 0);
  StartMotor (mpin10, mpin11, 0, 0);
  delay(ms);
}

void turn_right() {
  delay (500);
  delayStopped(500);
  StartMotor (mpin00, mpin01, 1, viteza_rotire);
  StartMotor (mpin10, mpin11, 0, viteza_rotire);

  delay (500);
  delayStopped(500);
}

void turn_left() {
  delay (500);
  delayStopped(500);
  StartMotor (mpin00, mpin01, 0, viteza_rotire);
  StartMotor (mpin10, mpin11, 1, viteza_rotire);

  delay (500);
  delayStopped(500);
}

void turn_around(){
  move_right();
  move_right();
  stop_moving();
}

void go_ahead_one() {
  StartMotor (mpin00, mpin01, 1, velocity + 5);
  StartMotor (mpin10, mpin11, 1, velocity);

  delay(1500);
}

void move_forward() { 
  digitalWrite(13, HIGH);
  digitalWrite(12, LOW);
  digitalWrite(8, LOW);
  digitalWrite(10, LOW);
  
  start_motors(1);
}

void move_backward() {
  digitalWrite(13, LOW);
  digitalWrite(12, HIGH);
  digitalWrite(8, LOW);
  digitalWrite(10, LOW);

  start_motors(0);
}

void move_left() {
  digitalWrite(13, LOW);
  digitalWrite(12, LOW);
  digitalWrite(8, HIGH);
  digitalWrite(10, LOW);

  turn_left();
}

void move_right() {
  digitalWrite(13, LOW);
  digitalWrite(12, LOW);
  digitalWrite(8, LOW);
  digitalWrite(10, HIGH);

  turn_right();
}

void stop_moving() {
  digitalWrite(13, LOW);
  digitalWrite(12, LOW);
  digitalWrite(8, LOW);
  digitalWrite(10, LOW);

  delayStopped(10);
}
