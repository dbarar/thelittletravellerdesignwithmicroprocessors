#include <Servo.h>
// Pinii motor 1
#define mpin00 6
#define mpin01 5
// Pinii motor 2
#define mpin10 11
#define mpin11 3

//Pinii pentru senzorul de distanta
#define infraRed A0
#define MAX_SIZE_STACK 200
#define TIME_IN_CASE_STOP 1000
#define TIME_TO_GO_FORWARD_IN_CASE_OBSTACLE 1000
#define TIME_TO_GO_ASIDE_IN_CASE_OBSTACLE 500
#define SERVO_DELAY 2000
int LED = 4; // Use the onboard Uno LED
int obstaclePin = 7;  // This is our input pin
int hasObstacle = HIGH;  // HIGH MEANS NO OBSTACLE

int distance_var = 0;
int velocity = 150;

Servo srv;
int servo_pin = 9;
int viteza_rotire = 115;

enum move_robot {
  go_forward,
  to_stop,
  go_left,
  go_right,
  go_back
};

char junk;
String inputString = "";

long start_time = 0;
long current_time = 0;
String  current_move = "S";

move_robot moves_stack[MAX_SIZE_STACK];
int top_moves_stack = 0;

long time_stack[MAX_SIZE_STACK];
int top_time_stack = 0;

void setup()                    // run once, when the sketch starts
{
  Serial.begin(9600);            // set the baud rate to 9600, same should be of your Serial Monitor
  
  // configurarea pinilor pentru leduri
  pinMode(13, OUTPUT);          //inainte
  pinMode(12, OUTPUT);          //inapoi
  pinMode(8, OUTPUT);           //stanga
  pinMode(10, OUTPUT);          //dreapta

  // configurarea pinilor motor ca iesire, initial valoare 0
  digitalWrite(mpin00, 0);
  digitalWrite(mpin01, 0);
  digitalWrite(mpin10, 0);
  digitalWrite(mpin11, 0);
  pinMode (mpin00, OUTPUT);
  pinMode (mpin01, OUTPUT);
  pinMode (mpin10, OUTPUT);
  pinMode (mpin11, OUTPUT);
  
}

void execute_move_back(move_robot a_move, long duration) {
  if (a_move == go_right) {
    move_left();
    delay(duration);
  } else if (a_move == go_left) {
    move_right();
    delay(duration);
  } else if (a_move == go_forward) {
    move_forward();
    //delay(duration);
    for(int i = 1; i < duration; i+=10){
      delay(10);
      hasObstacle = digitalRead(obstaclePin);
      if (hasObstacle == LOW){
        digitalWrite(LED, HIGH);
        stop_moving();
        delay(10);
        avoid_obstacle();
        duration = duration - TIME_TO_GO_FORWARD_IN_CASE_OBSTACLE;
        digitalWrite(LED, LOW);
        Serial.println(F("Obstacol"));
      }
    }
    
  } else if (a_move == go_back) {
    move_backward();
    delay(duration);
  } else if (a_move == to_stop) {
    stop_moving();
    delay(duration);
  }
}

void go() {
  if (inputString == "R") {
    //Serial.println(F("I will go right"));
    move_right();
  } else if (inputString == "L") {
    //Serial.println(F("I will go left"));
    move_left();
  } else if (inputString == "F") {
    //Serial.println(F("I will go forward"));
    move_forward();
  } else if (inputString == "B") {
    //Serial.println(F("I will go backward"));
    move_backward();
  } else {
    //Serial.println(F("I will go stop"));
    stop_moving();
  }
}

void come() {
  top_moves_stack--;
  top_time_stack--;
  while (top_moves_stack != -1) {
    move_robot m = pop_moves_stack();
    current_move =  move_robot2string(m);    
    current_time = pop_time_stack();
    Serial.println(current_move + "-" + current_time);

    //in ore to eliminate the noises
    if(current_time > 200) {
      execute_move_back(m, current_time);
      execute_move_back(to_stop, 200);
    }
  }
  top_moves_stack = 0;
  top_time_stack = 0;
}

void loop()
{
  if (Serial.available()) {
    while (Serial.available())
    {
      char inChar = (char)Serial.read(); //read the input
      inputString = inChar;        //make a string of the character coming on serial
    }
    while (Serial.available() > 0)
    {
      junk = Serial.read() ; // clear the serial buffer
    }
    //verificam daca nu suntem in mod intoarcere      
    if (inputString != "X" && inputString != "x") {
      if (inputString != current_move)
      {
        current_time = millis() - start_time;
        if(current_move != "S") {
          push_move_and_time();
        }
        start_time = millis();
        current_move = inputString;
        go();
      }
    }
    //modul intoarecere
    else {
      turn_around();
      come();
      current_move = "S";
    }
    inputString = "";
  }
}
