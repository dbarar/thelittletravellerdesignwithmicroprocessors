//void readDistance() {
//// distance_var = analogRead( );
// //Serial.println(distance_var);
//}

// Utilizare servo
// Pozitionare in trei unghiuri
// La final, ramane in mijloc (90 grade)
int obstacol_in_dreapta() {
  srv.write(0);
  delay(SERVO_DELAY);
  return not(digitalRead(obstaclePin));
}

int obstacol_in_stanga() {
  srv.write(180);
  delay(SERVO_DELAY);
  return not(digitalRead(obstaclePin));
}

void intoarce_servo_in_fata(){
  srv.write(90);
  delay(SERVO_DELAY);
}

int observe()
{
  srv.attach(servo_pin);

  int locatie = 0;
  if (not obstacol_in_dreapta())
    locatie = 1;
  else if (not obstacol_in_stanga())
    locatie = 2;
  else{
    locatie = 3;
  }
    intoarce_servo_in_fata();
    srv.detach();
    return locatie;
}


void avoid(int locatie) {
  //merge la dreapta
  if (locatie == 1) {
    move_right();
    move_forward();
    delay(TIME_TO_GO_ASIDE_IN_CASE_OBSTACLE);

    //depaseste paralel
    move_left();
    move_forward();
    delay(TIME_TO_GO_FORWARD_IN_CASE_OBSTACLE);

    //merge la stanga
    move_left();
    move_forward();
    delay(TIME_TO_GO_ASIDE_IN_CASE_OBSTACLE);

    //continua inainte
    move_right();
    move_forward();
  }
  else if (locatie = 2){
    move_left();
    move_forward();
    delay(TIME_TO_GO_ASIDE_IN_CASE_OBSTACLE);

    //depaseste paralel
    move_right();
    move_forward();
    delay(TIME_TO_GO_FORWARD_IN_CASE_OBSTACLE);

    //merge la stanga
    move_right();
    move_forward();
    delay(TIME_TO_GO_ASIDE_IN_CASE_OBSTACLE);

    //continua inainte
    move_left();
    move_forward();
  }
}

void avoid_obstacle() {
  int locatie = observe();
  Serial.println(locatie);
  avoid(locatie);
}

