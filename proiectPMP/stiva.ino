//stack for moves
int push_moves_stack(move_robot a_move) {
  if (top_moves_stack > MAX_SIZE_STACK || top_moves_stack < 0)
  {
    Serial.println(F("Stack overflow"));
    return -1;
  }
  else
  {
    moves_stack[top_moves_stack] = a_move;
    top_moves_stack = top_moves_stack + 1;
    return 0;
  }
}

move_robot pop_moves_stack() {
  if (top_moves_stack < 0) {
    Serial.println(F("Stack overflow"));
    return to_stop;
  }
  else {
    move_robot a_move = moves_stack[top_moves_stack];
    top_moves_stack = top_moves_stack - 1;
    return a_move;
  }
}

//stack for time
int push_time_stack(long a_time) {
  if (top_time_stack > MAX_SIZE_STACK || top_time_stack < 0)
  {
    Serial.println(F("Stack overflow"));
    return -1;
  }
  else
  {
    time_stack[top_time_stack] = a_time;
    top_time_stack = top_time_stack + 1;
    return 0;
  }
}

long pop_time_stack() {
  if (top_time_stack < 0) {
    Serial.println(F("Stack overflow"));
    return -1;
  }
  else {
    long a_time = time_stack[top_time_stack];
    top_time_stack = top_time_stack - 1;
    return a_time;
  }
}

move_robot string2move_robot(String a_move_string) {
  move_robot a_move_robot;
  
  if (a_move_string == "R")
    a_move_robot = go_right;
  if (a_move_string == "L")
    a_move_robot = go_left;
  if (a_move_string == "F")
    a_move_robot = go_forward;
  if (a_move_string == "B")
    a_move_robot = go_back;
  if (a_move_string == "S")
    a_move_robot = to_stop;
    
  return a_move_robot;
}

String move_robot2string(move_robot a_move_robot) {
  String string_robot;
  
  if (a_move_robot == go_right)
    string_robot = "R";
  if (a_move_robot == go_left)
    string_robot = "L";
  if (a_move_robot == go_forward)
    string_robot = "F";
  if (a_move_robot == go_back)
    string_robot = "B";
  if (a_move_robot == to_stop)
    string_robot = "S";

  return string_robot;
}

void push_move_and_time() {
  Serial.println(current_move + " - " + current_time + " - " + top_moves_stack);
  push_moves_stack(string2move_robot(current_move));
  push_time_stack(current_time);
}
